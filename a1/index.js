console.log("Hello Alpha")
console.log("Template Literals");
let num1 = 45;
const Cube = (num1)=> num1**3;
let getCube = Cube(num1);
console.log(`The cube ${num1} is ${getCube}`);



let address = ["3", "Agbannawag", "Tabuk City"]

const [purok, barangay, city] = address;

console.log("deconstruct");
console.log(`I live at Purok ${purok} ${barangay}, ${city}.`);

let animals = {
    name: "Chito",
    species: "Cheetah",
    weight: "125 pounds",
    measurement: "4 feet"
}

const {name: nm} = animals;
const {species: sp} = animals;
const {weight: wg} = animals;
const {measurement: msr} = animals;


console.log("deconstruct")
console.log(`${nm} was a ${sp}. He weighe at ${wg} with a mesurment of ${msr}.`);

console.log("Arrow Functions");
let number = [1,2,3,4];
number.forEach((num)=>{
    console.log(`${num}`)
})

class cat {
    constructor(name,age,breed){
        this.age = age;
        this.name = name;
        this.breed = breed;
}
}

const myCat = new cat("Nala", 2, "Puspin");

console.log(myCat);
