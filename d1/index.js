console.log("Hello Alpha")

//ES6 Updates
    //ES6 is one of the lates version of writing JS and in faction is one of the major updates
    //let and const
    //are ES6 updates too, these are new standards of creating variables


    var varSample = "Hoisst me up!!";
    console.log(varSample);

    var name = "Alpha"

    if(true){
        var name = "Hi!"
    }

    console.log("Hi my name is " + name)

//Exponent Operator

const firstNum = 8**2;
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

let eightPowerOfThree = 8**3;
console.log(eightPowerOfThree);

let string1 = "fun";
let string2 ="Bootcamp";
let string3 ="Coding";
let string4 ="JavaScript";
let string5 ="Zuitt";
let string6 ="Love";
let string7 ="Learning";
let string8 ="I";
let string9 ="is";
let string10 ="in";

let concatSentence1 = string8 + " " + string6 + " " + string4 + " " + string3 + " "+ string5 + " "+ string6 + " "+ string10 + " "+ string5 + ".";

console.log(concatSentence1)

//"",'' - string literals


let sentence1 = `${string8} ${string8}  ${string8}  ${string8}  ${string8}!` ;

console.log(sentence1);

let name2 = "John";

let message = "Hello" + name2 + "! Welcome to programming";

message = `hello ${name2}!`;

console.log(name2);

//Multi-line using Template literals 

const anotherMessage =  `


${name2} what is the you know ${firstNum}`;
console.log(anotherMessage)

let dev = {
    name: "Peter",
    lastName: "Parker",
    occupation: "web develeoper",
    income: 50000,
    expenses: 60000,
};

console.log(`${dev.name} is a ${dev.occupation}.`);
console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}`);

const interetRate = .1;
const principal = 1000;

console.log(`the interes on ${dev.name}'s saving acount is ${principal * interetRate}`);

const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
};

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

const {givenName, maidenName, familyName} = person;


console.log(givenName);
console.log(maidenName);
console.log(familyName);

//Object Destructuring

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName ({ givenName, maidenName, familyName}) {
    console.log(`${ givenName } ${ maidenName } ${ familyName }`);
}

getFullName(person);


let pokemon1 = {

	namePkmn: "Bulbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf","Tackle","Leech Seed"]

}

let {level,type,namePkmn,moves,personality} = pokemon1;

console.log(level);
console.log(type);
console.log(namePkmn);
console.log(moves);
console.log(personality);//undefined

let pokemon2 = {

	namePkmn: "Charmander",
	type: "Fire",
	level: 11,
	moves: ["Ember","Scratch"]

}

//{propertyName: newVariable}
const {namePkmn: nameVar} = pokemon2;
console.log(nameVar);

console.log(`${nameVar} goes char char!`)

const fullName = ["juan", "dela", "Cruz"];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}!`);

const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

let oneDirection = ["harry", "Zayn", "Nail", "Louis", "liam"];

let [singer1, , singer3, singer4] = oneDirection;
console.log(singer3);

//Traditional function

function displaymsg(){
    console.log("helo world")
}

displaymsg()


const hello = () => {
    console.log("helllooo")
}
hello()

const greet = (friend) => {
    console.log(`hi ${friend}`)
}

greet(singer3);
greet("alpha");

//Arrow vs Traditional function

//Implicit Return - allows us to return a value from an arrow function without the use of 


function addNum(num1,num2){
    let result = num1 + num2;
    return result
}

let sum = addNum(5,10);

console.log(sum);

let subNum = (num1,num2) => num1 - num2;

let difference = subNum(10,5);
console.log(difference);

let addition = (num1,num2) => num1 + num2;
let subtractrion = (num1,num2) => num1 - num2;
let multiplication = (num1,num2) => num1 * num2;
let divission = (num1,num2) => num1 / num2;

let sum1 = addition(7,5);
let difference1 = subtractrion(7,5);
let product = multiplication(7,5);
let quotient = divission(7,5);

console.log(sum1);
console.log(difference1);
console.log(product);
console.log(quotient);

const bears = ["Morgan", "Amy", "Lulu"];

bears.forEach(function(bear){
    console.log(`${bear} is my bestfriend`)
})
bears.forEach((bear)=>{
    console.log(`${bear} is my bestbody`)
})

const messageForYou = (name = 'Cardo')=>{
    return `good evening ${name}`
}
console.log(messageForYou());
console.log(messageForYou("alpha"));

//Class-Based Object Blueprints

class Car {
    constructor(brand,name,year){
        this.brand =brand;
        this.name = name;
        this.year = year;
    }
}

const myCar = new Car();

console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Everest";
myCar.year = "1996";
console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", "2020")

console.log(myNewCar);

class Character {
    constructor(name,role,strength,weakness){
        this.role =role;
        this.name = name;
        this.strength = strength;
        this.weakness = weakness;
        this.introduce = () => {
            console.log(`Hi! I am ${this.name}`)
        }
    }
}

const onePeace = new Character("Luffy","Captain","Immense physical strength", "Can't swim");

console.log(onePeace);

